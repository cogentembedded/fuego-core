   {
    "testName": "Benchmark.Dhrystone",
    "fail_case": [
        {
            "fail_regexp": "Measured time too small to obtain meaningful results",
            "fail_message": "Measured time too small to obtain meaningful results. Please increase LOOPS parameter in Dhrystone test spec."
            }
        ],
    "specs": 
    [
        {
            "name":"default",
            "LOOPS":"10000000"
        }
    ]
}
 
