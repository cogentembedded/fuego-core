tarball=hackbench.tar.gz

function test_build {
    $CC -lpthread hackbench.c -o hackbench && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put hackbench  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./hackbench $groups"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
