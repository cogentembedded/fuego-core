#!/bin/bash

tarball=bc-script.tar.gz

function test_build {
    echo "test compiling (should be here)"
}

function test_deploy {
	put bc-device.sh  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
    assert_define FUNCTIONAL_BC_EXPR
    assert_define FUNCTIONAL_BC_RESULT
    report "cd $FUEGO_HOME/fuego.$TESTDIR; ./bc-device.sh $FUNCTIONAL_BC_EXPR $FUNCTIONAL_BC_RESULT"  
}

function test_processing {
    log_compare "$TESTDIR" "1" "OK" "p"          
}

. $FUEGO_SCRIPTS_PATH/functional.sh
