tarball=ipv6connect.tar.gz

function test_build {
    make CC="$CC" LD="$LD" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put ipv6connect  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./ipv6connect"  
}

function test_processing {
	true
}

. $FUEGO_SCRIPTS_PATH/functional.sh
