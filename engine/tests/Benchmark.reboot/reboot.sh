tarball=reboot

function test_build {
	true
}

function test_deploy {
	put $TEST_HOME/$tarball  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	# MAX_REBOOT_RETRIES can be defined in the board's file. 
	# Otherwise, the default is 10 retries
	retries=${MAX_REBOOT_RETRIES:-10}
	target_reboot $retries
	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./reboot"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
