source $FUEGO_SCRIPTS_PATH/overlays.sh
set_overlay_vars

source $FUEGO_SCRIPTS_PATH/reports.sh
source $FUEGO_SCRIPTS_PATH/functions.sh

source $TEST_HOME/../LTP/ltp.sh

function test_run {
	report "cd /tmp/fuego.$TESTDIR/target_bin; ./bin/run-all-posix-option-group-tests.sh"
}

function test_processing {
    assert_define LTP_OPEN_POSIX_SUBTEST_COUNT_POS
    assert_define LTP_OPEN_POSIX_SUBTEST_COUNT_NEG
    
    P_CRIT="execution: PASS"
    N_CRIT="execution: (FAIL|UNSUPPORTED|SIGNALED|UNTESTED|SKIPPED|UNRESOLVED|EXITED ABNORMALLY)"

    log_compare "$TESTDIR" $LTP_OPEN_POSIX_SUBTEST_COUNT_POS "${P_CRIT}" "p"
    log_compare "$TESTDIR" $LTP_OPEN_POSIX_SUBTEST_COUNT_NEG "${N_CRIT}" "n"
}

test_run
get_testlog $TESTDIR
test_processing
