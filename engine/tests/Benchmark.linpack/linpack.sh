tarball=linpack.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/linpack.c.patch
    $CC $CFLAGS -O -lm -o linpack linpack.c && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put linpack  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR && ./linpack"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
